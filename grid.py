# grid.py


class Grid(object):
    up =    ( 0, -1)
    down =  ( 0,  1)
    left =  (-1,  0)
    right = ( 1,  0)

    directions = (up, down, left, right)

    def __init__(self, width=2, height=2, index=0):
        self.width = width
        self.height = height
        self.index = index
        self.wrap = False
        self.cells = [[ Cell(x, y, grid=self) for x in range(width)  ]
                                              for y in range(height) ]

    def __str__(self):
        return "Grid (%s, %s) Indexed: %s" % \
               (self.width, self.height, self.index)

    @property
    def rows(self):
        return self.cells

    @property
    def columns(self):
        return [[ self.cells[y][x]
                  for y in range(self.height) ]
                  for x in range(self.width)  ]

    def column_at(self, x):
        return self.columns[x]

    def explain(self):
        for row in self.rows:
            print()
            for cell in row:
                print(cell, end='')
        print()

    @property
    def flattened(self):
        flat = []
        for row in self.rows:
            for cell in row:
                flat.append(cell)
        return flat


class Cell(object):
    def __init__(self, x, y, grid=None):
        self.grid = grid
        self.x = x
        self.y = y

    def __str__(self):
        return "Cell (%s, %s)" % (self.x, self.y)

    @property
    def column(self):
        return self.grid.column_at(self.x)

    @property
    def row(self):
        return self.grid.cells[self.y]

    @property
    def pos(self):
        return (self.x, self.y)

    def neighbor(self, direction):
        pos = (self.y + direction[1], self.x + direction[0])
        #print('pos', pos)

        if pos[0] < 0 or \
           pos[1] < 0 or \
           pos[0] >= self.grid.width or \
           pos[1] >= self.grid.height:
            wrap = True
        else:
            wrap = False

        if not self.grid.wrap and wrap:
            return

        return self.grid.cells[pos[0]][pos[1]]

    @property
    def neighbors(self):
        cells = []
        for direction in Grid.directions:
            #print('direction', direction)
            #print('neighbor', self.neighbor(direction))
            #print()
            neighbor = self.neighbor(direction)
            if neighbor:
                cells.append(neighbor)
        return cells
