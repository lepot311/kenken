from bottle import route, run

import kenken


@route('/kenken/new/<size:re:[3-9]?>')
def game(size):
    if size:
        puzzle = kenken.KK(int(size))
    else:
        puzzle = kenken.KK(4)

    puzzle.populate()
    puzzle.create_groups()
    return [ puzzle.as_json.encode('ascii') ]


run(host='localhost', port='55555')
