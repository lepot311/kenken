# kenken.py

import functools
import itertools
import json
import operator
import random
import sys

import termcolor

import grid

DEBUG_LEVEL = 1

def debug(level, *args, end='\n'):
    if DEBUG_LEVEL >= level:
        print(*args, end=end)

class NumberException(Exception): pass

class KKCell(grid.Cell):
    def __init__(self, x, y, grid=None):
        super().__init__(x, y, grid)
        self.__answer = None
        self.group = None

    @property
    def answer(self):
        return self.__answer

    @answer.setter
    def answer(self, value):
        #debug('setting', self, 'to', value)
        row = [ cell.answer for cell in self.row ]
        col = [ cell.answer for cell in self.column ]

        if value is None:
            pass
        elif value < self.grid.puzzle.number_min:
            raise NumberException(
                "Number must be greater than %s" % (self.number_min - 1))
        elif value > self.grid.puzzle.number_max:
            raise NumberException(
                "Number must be less than %s" % (self.number_max + 1))
        elif value in row:
            raise NumberException(
                "Number %s already exists in row %s" %
                    (value, self.y + 1))
        elif value in col:
            raise NumberException(
                "Number %s already exists in column %s" %
                    (value, self.x + 1))
        self.__answer = value

    @property
    def in_group(self):
        return True

    def _color(self, str):
        dark = False

        if self.group:
            color = 'on_%s' % self.group.color
        else:
            color = None

        if dark:
            if str == '-':
                return termcolor.colored(' ', 'white', 'on_cyan')
            else:
                return str
        else:
            if str == '-':
                return str
            else:
                return termcolor.colored(str, 'white', color)


    @property
    def _top(self):
        up = KKGrid.directions[0]
        string = ''
        string += self._color('-')
        #string += '%s%s ' % self.pos
        neighbor = self.neighbor(up)
        if not self.group \
            or not neighbor \
            or not neighbor.group is self.group:
            string += self._color('-')
        else:
            string += self._color(' ')
        string += self._color('-')
        return string

    @property
    def _bottom(self):
        down = KKGrid.directions[1]
        string = ''
        string += self._color('-')
        neighbor = self.neighbor(down)
        if not self.group \
            or not neighbor \
            or not neighbor.group is self.group:
            string += self._color('-')
        else:
            string += self._color(' ')
        string += self._color('-')
        return string

    @property
    def _middle(self):
        left = KKGrid.directions[2]
        right = KKGrid.directions[3]
        string = ''

        neighbor = self.neighbor(left)
        if not self.group \
            or not neighbor \
            or not neighbor.group is self.group:
            string += self._color('-')
        else:
            string += self._color(' ')

        #string += self._color(str(self.answer))
        if len(self.group.cells) == 1:
            string += self._color(str(self.answer))
        else:
            string += self._color('*')

        neighbor = self.neighbor(right)
        if not self.group \
            or not neighbor \
            or not neighbor.group is self.group:
            string += self._color('-')
        else:
            string += self._color(' ')
        return string


class KKGroup(object):
    operations = {
        '+': operator.add,
        '-': operator.sub,
        'x': operator.mul,
        #'/': operator.floordiv,
    }
    colors = itertools.cycle(['red', 'green', 'yellow', 'blue', 'magenta', 'cyan'])

    def __init__(self, cells):
        self.cells = cells
        self.color = None
        self.op = None
        self.group_id = None

        if len(self.cells) > 1:
            self.op = random.choice(list(KKGroup.operations.items()))
            self.answer = functools.reduce(self.op[1], sorted([cell.answer for cell in self.cells]))
        else:
            self.answer = self.cells[0].answer

        #print('Made new group of %s cells' % len(cells))
        #print('  ', [(cell.x, cell.y) for cell in cells])

    def explain(self):
        print()
        print('Group:')
        print('  Length:', len(self.cells))
        print('  Operation:', self.op)
        print('  Answer:', self.answer)
        print('  Cells:', [(cell.pos, cell.answer) for cell in self.cells])


class KKGrid(grid.Grid):
    def __init__(self, puzzle, size=4):
        super().__init__(width=size, height=size, index=1)
        self.puzzle = puzzle
        self.size = size
        self.cells = [[ KKCell(x, y, grid=self) for x in range(size) ]
                                                for y in range(size) ]


class KK(object):
    def __init__(self, size=4):
        self.grid = KKGrid(self, size)
        self.number_min = 1
        self.number_max = self.grid.size
        self.valid_numbers = set(range(self.number_min, self.number_max + 1))
        self.groups = []
        self.group_ids = itertools.count()
        debug(1, "New KK of size", size)

    def __str__(self):
        return self.grid

    @property
    def rows(self):
        return self.grid.rows

    def populate(self):
        for rn, row in enumerate(self.rows):
            self.set_next_cell(rn, 0)

            debug(2, 'Successfully filled row', rn + 1)
            debug(2)
            if DEBUG_LEVEL >= 2:
                self.explain()

    def explain(self):
        for row in self.rows:
            print()
            fmt = "[ {0:%s} ]" % len(str(self.grid.size))
            for cell in row:
                answer = cell.answer or '--'
                print(fmt.format(answer), end='')
        print()

    def explain_groups(self):
        print()

        for row in self.rows:
            for cell in row:
                print(cell._top, end='')
            print()
            for cell in row:
                print(cell._middle, end='')
            print()
            for cell in row:
                print(cell._bottom, end='')
            print()


    def set_next_cell(self, rn, cn):
        tried = set()
        possible = True
        last_in_row = self.grid.cells[rn][self.grid.size - 1]

        while possible and not last_in_row.answer:
            debug(3)

            debug(3, "setting column", cn + 1)
            cell = self.grid.cells[rn][cn]

            in_row = set([cell.answer for cell in self.grid.cells[rn]])
            debug(3, "in row", in_row)

            in_col = set([cell.answer for cell in self.grid.column_at(cn)])
            debug(3, "in col", in_col)

            possible = set(self.valid_numbers) - in_row - in_col - tried
            debug(3, "possible", possible)

            choice = random.choice(list(possible))
            debug(3, "choice was %s" % choice)

            tried.add(choice)

            try:
                cell.answer = choice
                debug(3, "choice worked")

                if cn <= self.grid.size - 2:
                    try:
                        self.set_next_cell(rn, cn + 1)
                    except IndexError as e:
                        debug(2, e)
                        cell.answer = None
                else:
                    debug(2)
                    debug(2, 'done')
            except NumberException as e:
                debug(2, e)
                cell.answer = None

    @property
    def cells_without_group(self):
        return [ cell for cell in self.grid.flattened if cell.group is None ]

    def create_group(self, head):
        #print(len(self.cells_without_group), 'without a group')
        #print([(cell.pos, cell.answer) for cell in self.cells_without_group])
        #print('Creating group')

        group = None

        while not group \
              or group.answer < 0: # disallow negatives

            #print('  Head', head)
            cells = [ cell for cell in head.neighbors if cell.group is None ]
            cells.append(head)
            group = KKGroup(cells)

        group.color = next(KKGroup.colors)
        for cell in group.cells:
            cell.group = group

        group.group_id = next(self.group_ids)

        if DEBUG_LEVEL > 2:
            group.explain()

        self.groups.append(group)

    def create_groups(self):
        while self.cells_without_group:
            head = random.choice(self.cells_without_group)
            self.create_group(head)

    @property
    def key(self):
        print()
        for group in self.groups:
            if group.op:
                print(termcolor.colored('  ', 'white', 'on_%s' % group.color), end=' ')
                print(group.op[0], group.answer)

    @property
    def as_json(self):
        groups_seen = set()
        result = {
            'puzzle': {
                'cells': [],
                'n_groups': len(self.groups),
                'size': self.grid.size,
            }
        }
        for row in self.grid.rows:
            r = []
            for cell in row:
                c = {
                    'x':      cell.x,
                    'y':      cell.y,
                    'answer': cell.answer,
                    'guess':  None,
                    'border': {},
                    'group':  {
                        'color':  cell.group.color,
                        'answer': cell.group.answer,
                        'first':  True,
                    }
                }

                if cell.group.group_id in groups_seen:
                    c['group']['first'] = False
                groups_seen.add(cell.group.group_id)

                if cell.group.op:
                    c['group']['op'] = cell.group.op[0]

                for name, direction in zip(['up', 'down', 'left', 'right'], KKGrid.directions):
                    if cell.neighbor(direction) \
                        and cell.neighbor(direction).group \
                        and not cell.neighbor(direction).group is cell.group:
                        c['border'][name] = True
                    else:
                        c['border'][name] = False

                r.append(c)
            result['puzzle']['cells'].append(r)
        return json.dumps(result)


if __name__ == '__main__':
    try:
        size = int(sys.argv[1])
    except IndexError:
        size = 4
    puzzle = KK(size=size)
    puzzle.populate()
    puzzle.create_groups()
    puzzle.explain()
    puzzle.explain_groups()
    print(puzzle.key)
    print(len(puzzle.grid.flattened), 'cells total')
