import grid


def test_columns(grid):
    assert grid.column_at(0) == ['A', 'E', 'I', 'M']
    assert grid.column_at(1) == ['B', 'F', 'J', 'N']
    assert grid.column_at(2) == ['C', 'G', 'K', 'O']
    assert grid.column_at(3) == ['D', 'H', 'L', 'P']


def runtests():
    g = grid.Grid(width=4, height=4)
    g.data = [
        ['A', 'B', 'C', 'D'],
        ['E', 'F', 'G', 'H'],
        ['I', 'J', 'K', 'L'],
        ['M', 'N', 'O', 'P'],
    ]
    test_columns(g)


if __name__ == '__main__':
    runtests()
