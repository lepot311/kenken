var app = angular.module('KenKenApp', []);

app.controller('KenKenCtrl', function ($scope, $http) {
  $scope.name = 'Erik'

  $scope.new = function(size) {
    var url = size ? '/kenken/new/' + size : '/kenken/new/4'
    $http.get(url)
      .success(function(response) {
        var puzzle;
        puzzle = response.puzzle;
        $scope.puzzle = puzzle;
        $scope.puzzle_size = puzzle.size;
      })
  }

  $scope.new();

});
